<?php

/**
 * Plugin Name: Luxafor
 * Description: Luxafor Light Alert
 * Version: 1.0
 * Author: DK
 * Author URI: https://gitlab.com/idevaton
 */

// adding initial template
add_filter( 'template_include', 'luxafor_template', 99 );
function luxafor_template( $template ) {
    if ( get_query_var( 'luxafor_webhook_id' ) ) {
        $template = get_template_directory() . '/page-templates/template-luxafor.php';
    }
    return $template;
}

// function that runs when shortcode is called, shortcode will be included in the wordpress page
function sendcolor() {
    ob_start();
    require_once ( plugin_dir_path(__FILE__) . '/sendcolor.php');
    $content = ob_get_clean();
    return $content;
}

// function that runs when shortcode is called
function jira() { 
    
    ob_start();
    require_once ( plugin_dir_path(__FILE__) . '/jira.php');
    $content = ob_get_clean();
    return $content;
}

// register shortcodes
add_shortcode('sendcolor', 'sendcolor');
add_shortcode('jira', 'jira');

// adding url params
add_filter('query_vars', 'add_query_vars');
function add_query_vars($aVars) 
{
    $aVars[] = "LuxaforFlagId";
    $aVars[] = "Color";
    $aVars[] = "OrgName";
    $aVars[] = "UserName";
    $aVars[] = "APIToken";
    return $aVars;
}

function luxafor_get_api_json_response($color, $luxaforFlagId)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.luxafor.com/webhook/v1/actions/solid_color',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{ "userId": "' . $luxaforFlagId . '", "actionFields":{ "color": "' . $color . '" }}',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));
    
    $jsonresponse = curl_exec($curl);
    curl_close($curl);
}

function luxafor_check_jira($jiraOrgName, $jiraUserName, $jiraApiToken, $luxaforFlagId) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://' . $jiraOrgName . '.atlassian.net/rest/api/2/search?jql=project=LT%20AND%20status=%22New%22&fields=status',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
          'Accept: application/json',
          'Authorization: Basic '. base64_encode($jiraUserName . ':' . $jiraApiToken)
        ),
    ));

    $response = curl_exec($curl);

    $obj = json_decode($response);
    if (empty($obj->{'issues'})) {
        // No open issues, set the light to green
        luxafor_get_api_json_response('green', $luxaforFlagId);
    }else{
        // open issues present, set the light to red
        luxafor_get_api_json_response('red', $luxaforFlagId);
    }
    curl_close($curl);
}

?>
