<?php /* Template Name: Luxafor */

    $colorSent = false;
    $color = get_query_var('Color');
    $luxaforFlagId = get_query_var('LuxaforFlagId');

    if(!empty($color) && !empty($luxaforFlagId))
    {
        $colorSent = true;
        luxafor_get_api_json_response($color, $luxaforFlagId);
    }
?>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<div class="jumbotron">
    <form id="luxafor-send-color" method="GET" role="tabpanel" autocomplete="off">

        <?php if($colorSent) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissible">
                        <p>Color: <?= $color ?> sent!</p>
                    </div>
                </div>
            </div>
        <?php } ?>

        <h2>Changing Colors Manually</h2>
            <div class="col-md-6">
                <h3>Luxafor Webhook Id</h3>
                <input class="form-control" data-val="true" data-val-required="The Luxafor Flag Id field is required." id="LuxaforFlagId" name="LuxaforFlagId" type="text" value="<?= $luxaforFlagId ?>">
                <span class="field-validation-valid text-danger" data-valmsg-for="LuxaforFlagId" data-valmsg-replace="true"></span>
            </div>
            <div class="col-md-6">
                <h3>Pick Color</h3>
                <select class="form-control" data-val="true" data-val-required="The Color field is required." id="Color" name="Color">
                    <option selected="selected" value="blue">Blue</option>
                    <option value="cyan">Cyan</option>
                    <option value="green">Green</option>
                    <option value="magenta">Magenta</option>
                    <option value="red">Red</option>
                    <option value="white">White</option>
                    <option value="yellow">Yellow</option>
                </select>
                <span class="field-validation-valid text-danger" data-valmsg-for="Color" data-valmsg-replace="true"></span>
            </div>
        </div>
        <div class="row" style="padding-top: 15px;">
            <div class="col-md-12">
                <p><input type="submit" value="Send" class="btn btn-default"></p>
                <p class="bg-warning p-2">Note: it may take few seconds to get response and for the color of the flag to change. If not, try to switch to Webhook tab in the Luxafor Desktop Application, or restart all services.</p>
                <p><img class="img-responsive" src="<?php echo plugin_dir_url( __FILE__ ) . 'images/luxafor-wbehook-id.JPG'; ?>"></p>
            </div>
        </div>
    </form>
</div>