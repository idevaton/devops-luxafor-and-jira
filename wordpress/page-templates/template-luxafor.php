<?php /* Template Name: Luxafor */
?>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h2>Luxafor Flag</h2>
            <p class="lead">Changing colors of Luxafor flag manually or based on changes in JIRA.</p>
            <p><a href="https://luxafor.com/" target="_blank" class="btn btn-primary btn-lg">Luxafor Site &raquo;</a></p>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3>Changing Colors Manually</h3>
                <p><a class="btn btn-info" href="/index.php/send-color">View Example &raquo;</a></p>
            </div>
            <div class="col-md-6">
                <h3>Changing Colors Based On Changes In JIRA</h3>
                <p><a class="btn btn-info" href="/index.php/jira">View Example &raquo;</a></p>
            </div>
        </div>
        <div class="row" style="padding-top: 25px;">
            <div class="col-md-12">
                <p class="alert-warning" style="font-size: 20px; padding:15px;">
                    This project assumes that you have an operational JIRA Software and Luxafor flag device plugged into your PC or Mac and can obtain Webhook Luxafor Id from the Luxafor Desktop Application to make this work.
                </p>
            </div>
        </div>
    </div>
</body>
