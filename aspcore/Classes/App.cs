﻿using aspcore.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace aspcore.Classes
{
    public class AppFunctions
    {
        public static LuxaforFlagResult CallLuxaforFlag(string luxaforId, string flagColor)
        {
            LuxaforFlagResult result = new LuxaforFlagResult
            {
                Result = false,
                Message = "N/A"
            };

            try
            {
                var client = new RestClient("https://api.luxafor.com/webhook/v1/actions/solid_color")
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "{\r\n  \"userId\": \"" + luxaforId + "\",\r\n  \"actionFields\":{\r\n    \"color\": \"" + flagColor + "\"\r\n  }\r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                result.Message = "Success, color changed: " + response.Content.ToString();
                result.Result = true;
            }
            catch(Exception e)
            {
                // Log error here
                result.Message = e.Message.ToString();
            }
            return result;
        }

        public static LuxaforFlagResult CheckJira(string luxaforId, string jiraOrgName, string jiraApiToken, string jiraUserName)
        {
            LuxaforFlagResult result = new LuxaforFlagResult
            {
                Result = false,
                Message = "N/A"
            };

            try
            {
                string jiraSearchUrl = "https://" + jiraOrgName + ".atlassian.net/rest/api/2/search?jql=project=LT%20AND%20status=%22New%22&fields=status";
                try
                {
                    string jsonResult = PostJsonRequest(jiraSearchUrl, jiraUserName, jiraApiToken);
                    var jo = JObject.Parse(jsonResult);
                    var joIssues = jo["issues"];
                    if (joIssues != null)
                    {
                        List<JiraTicket> issues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<JiraTicket>>(joIssues.ToString());
                        if (issues != null && issues.Count > 0)
                        {
                            // set red color
                            result = AppFunctions.CallLuxaforFlag(luxaforId, "red");
                        }
                        else
                        {
                            // set green color
                            result = AppFunctions.CallLuxaforFlag(luxaforId, "green");
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Message = e.Message.ToString();
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message.ToString();
            }
            return result;
        }

        private static string GetEncodedCredentials(string jiraUserName, string jiraApiToken)
        {
            string mergedCredentials = string.Format("{0}:{1}", jiraUserName, jiraApiToken);
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }

        public static string PostJsonRequest(string jiraSearchUrl, string jiraUserName, string jiraApiToken)
        {
            // Create string to hold JSON response
            string jsonResponse = string.Empty;
            using (var client = new WebClient())
            {
                try
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Set("Authorization", "Basic " + GetEncodedCredentials(jiraUserName, jiraApiToken));
                    client.Headers.Add("Content-Type: application/json");
                    client.Headers.Add("Accept", "application/json");
                    var response = client.DownloadString(jiraSearchUrl);
                    jsonResponse = response;
                }
                catch (WebException ex)
                {
                    // HTTP Error
                    if (ex.Status == WebExceptionStatus.ProtocolError)
                    {
                        HttpWebResponse wrsp = (HttpWebResponse)ex.Response;
                        var statusCode = (int)wrsp.StatusCode;
                        var msg = wrsp.StatusDescription;
                        //throw new HttpBadRequest(400);
                    }
                    else
                    {
                        //throw new HttpException(500, ex.Message);
                    }
                }
            }
            return jsonResponse;
        }
    }
}