﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace azurecanary
{
    internal class Program
    {
        private static string azureAuthToken = string.Empty;
        public static async Task Main(string[] args)
        {
            string luxaforId = ""; // ENTER YOUR VALUE
            var httpClient = new HttpClient();
            bool allGood = true;

            Console.WriteLine("Start Program: " + DateTime.Now);
            try
            {
                while (true)
                {
                    Console.WriteLine("No Errors: " + DateTime.Now);
                    if (allGood)
                    {
                        await SendColorAsync(luxaforId, "green");
                    }
                    else
                    {
                        await SendColorAsync(luxaforId, "red");
                    }
                    
                    // Now lets check Azure Subscription
                    List<AzureConfig> azureConfigs = new List<AzureConfig>();

                    // add your azure details
                    // IMPORTANT: make sure you add application to Access Control of the Subscription
                    azureConfigs.Add(new AzureConfig
                    {
                        ApplicationId = "", // ENTER YOUR VALUE
                        ApplicationSecret = "", // ENTER YOUR VALUE
                        TenantId = "", // ENTER YOUR VALUE
                        AuthEndPointResource = "https://management.azure.com/",
                        AuthorityUrl = "https://login.microsoftonline.com/{tenantid}/oauth2/token",
                        Scope = "https://management.azure.com/.default"
                    });

                    if(azureConfigs != null && azureConfigs.Count > 0)
                    {
                        foreach (AzureConfig azureConfig in azureConfigs)
                        {
                            // For app only authentication, we need the specific tenant id and end point
                            var tenantSpecificURL = azureConfig.AuthorityUrl.Replace("{tenantid}", azureConfig.TenantId);
                            httpClient = new HttpClient();
                            var parameters = new Dictionary<string, string> { 
                                  { "grant_type", "client_credentials" }
                                , { "client_id", azureConfig.ApplicationId }
                                , { "client_secret", azureConfig.ApplicationSecret }
                                , { "resource", azureConfig.AuthEndPointResource }
                            };
                            var encodedContent = new FormUrlEncodedContent(parameters);

                            var response = await httpClient.PostAsync(tenantSpecificURL, encodedContent);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                await SendColorAsync(luxaforId, "green");
                                allGood = true;

                                var content = await response.Content.ReadAsStringAsync();
                                AzureAuth azureAuth = JsonConvert.DeserializeObject<AzureAuth>(content);
                                azureAuthToken = azureAuth.access_token;

                                httpClient = new HttpClient();
                                httpClient.DefaultRequestHeaders.Authorization =
                                    new AuthenticationHeaderValue("Bearer", azureAuthToken);

                                string subUrl = "https://management.azure.com/subscriptions?api-version=2020-01-01";
                                HttpResponseMessage response2 = await httpClient.GetAsync(subUrl);
                                if (response2.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    await SendColorAsync(luxaforId, "green");
                                    allGood = true;

                                    var content2 = await response2.Content.ReadAsStringAsync();

                                    //Check Subscription State
                                }
                                else
                                {
                                    await SendColorAsync(luxaforId, "red");
                                    allGood = false;
                                }
                            }
                            else
                            {
                                await SendColorAsync(luxaforId, "red");
                                allGood = false;
                            }
                        }
                    }
                    // sleep for 30 seconds
                    //Thread.Sleep(30000);

                    // sleep for 10 seconds
                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("End Program: " + DateTime.Now);
        }

        public static async Task SendColorAsync(string luxaforId, string color)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

            var luxFlag = new luxaforFlagRequest()
            {
                actionFields = new luxaforFlagColor
                {
                    color = color
                },
                userId = luxaforId
            };

            var json = JsonConvert.SerializeObject(luxFlag);
            HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await httpClient.PostAsync(string.Format("https://api.luxafor.com/webhook/v1/actions/solid_color"), httpContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync();
            }
            else
            {
                // TODO: log error or send email that there is an issue
            }
        }
    }
    
    //References:
    //https://docs.microsoft.com/en-us/rest/api/resources/subscriptions/list
    //https://docs.microsoft.com/en-us/rest/api/managementgroups/management-group-subscriptions/get-subscription

    //more on token
    //https://medium.com/azure-services/getting-an-access-token-in-azure-using-c-faf98e218790
    //https://stackoverflow.com/questions/46194170/invalidauthenticationtoken-error-with-microsoft-graph/46195594#46195594
    //https://stackoverflow.com/questions/59279543/unauthorized-issue-when-request-user-list-to-microsoft-graph-by-using-httpclient
    //https://stackoverflow.com/questions/38262085/azure-ad-api-request-401-unauthorized/70827086#70827086
}
