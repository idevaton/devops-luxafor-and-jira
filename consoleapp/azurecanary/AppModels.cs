﻿namespace azurecanary
{
    // IMPORTANT: do not change luxafor attribute names, names must start with lower case
    // if names do not match then you might see unauthorized errors
    public class luxaforFlagRequest
    {
        public string userId { get; set; }
        public luxaforFlagColor actionFields { get; set; }
    }

    public class luxaforFlagColor
    {
        public string color { get; set; }
    }

    public class AzureConfig
    {
        public string ApplicationId { get; set; }
        public string TenantId { get; set; }
        public string ApplicationSecret { get; set; }
        public string AuthEndPointResource { get; set; }
        public string AuthorityUrl { get; set; }
        public string Scope { get; set; }
    }

    public class AzureAuth
    {
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string ext_expires_in { get; set; }
        public string expires_on { get; set; }
        public string not_before { get; set; }
        public string resource { get; set; }
        public string access_token { get; set; }
    }
}
